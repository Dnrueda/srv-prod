package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.client.*;
import com.mongodb.MongoClientSettings;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {

    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        //creamos ruta para conectarnos a nuestra bd mongo
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");

        //definición de como se comportará el cliente de mongo
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        //creación del cliente
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("BackMayo");
        return database.getCollection("servicios");
    }
    //select all
    public static List getAll(){
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        // se solicitan todos los servicios
        FindIterable<Document> iterDoc = servicios.find();
        Iterator iterator = iterDoc.iterator();
        while (iterator.hasNext()){
            list.add(iterator.next());
        }
        return list;
    }
    //insert
    public static void insert(String cadenaServicio) throws Exception {
        //transforma JSON a la cadena Document
        Document doc = Document.parse(cadenaServicio);
        //puntero a la coleccion
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    //insert batch de varios servicios a la vez
    public static void insertBatch(String cadenaServicios) throws Exception {
        //transforma JSON a la cadena Document
        Document doc = Document.parse(cadenaServicios);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        //puntero a la coleccion
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);
    }

    //busqueda por filtro
    public static List<Document> getFiltrados(String cadenaFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);

        // se solicitan todos los servicios
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterator = iterDoc.iterator();
        while (iterator.hasNext()){
            lista.add(iterator.next());
        }
        return lista;
    }
    public static List<Document> getFiltradosPeriodo(Document docFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();

        // se solicitan todos los servicios
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterator = iterDoc.iterator();
        while (iterator.hasNext()){
            lista.add(iterator.next());
        }
        return lista;
    }

    //funcion para updatear
    public static void update(String filtro, String valores){
        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        servicios.updateOne(docFiltro, docValores);

    }
}
