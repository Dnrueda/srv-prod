package com.techuniversity.prod.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

//se trata del repositorio de la base de datos extends es para heredar
@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {

}
