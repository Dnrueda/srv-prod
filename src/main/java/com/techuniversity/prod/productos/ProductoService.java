package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

//aqui se hace la interaccion con la base de datos
@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    //funcion para recuperar los objetos de la coleccion
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    //encontramos por id
    public Optional<ProductoModel> findByid(String id){
        return productoRepository.findById(id);
    }

    //insertamos el JSON q nos llegue dandoles el nuevo id o updatear
    public ProductoModel save(ProductoModel producto) {
        return productoRepository.save(producto); //con la función Save decide si se hace insert o update
    }

    //función borrado
    public boolean deleteProducto(ProductoModel producto){
        try{
            productoRepository.delete(producto);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    //paginacion (informacion paginada)

    public List<ProductoModel> findPaginado(int page){
        Pageable pageable = PageRequest.of(page, 3);
        Page<ProductoModel> pages = productoRepository.findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }


}
